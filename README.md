# Simple Spring Kafka example

This Spring Boot project contains the logic needed for receiving and sending events 
from and to an Apache Kafka server using String and JSON values.

Please note that in order to test the project you need to install, configure and run an 
Apache Kafka on the machine running this project. 

If you want to run Kafka on another machine, 
you will need to modify the value of apache.kafka.server.address, found inside the properties file.

Also note that your Apache server will need to match the topics declared within the KafkaConsumerConfiguration class. 

