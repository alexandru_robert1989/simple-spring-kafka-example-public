package com.example.simplespringkafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = {"com.*"})
@ComponentScan(basePackages = {"com.*"})
public class SimpleSpringKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleSpringKafkaApplication.class, args);
    }

}
