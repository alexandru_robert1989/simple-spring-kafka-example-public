package com.example.simplespringkafka.config;

import com.example.simplespringkafka.model.User;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Configuration for the Kafka Consumer. The ConsumerFactory objects provide the logic needed
 * for serializing and deserializing the events captured by the ConcurrentKafkaListenerContainerFactory.
 */
@EnableKafka
@Configuration
public class KafkaConsumerConfiguration {


    @Value("${apache.kafka.server.address}")
    private String kafkaServerAddress;

    @Value("${apache.kafka.default.group-id}")
    private String kafkaDefaultGroupId;

    @Value("${apache.kafka.json-group-id}")
    private String kafkaJsonGroupId;


    /**
     * Bean representing a ConsumerFactory with a String key and a String value
     *
     * @return a {@link ConsumerFactory} with a String key and a String value
     */
    @Bean
    public ConsumerFactory<String, String> stringConsumerFactory() {

        // map that will contain the consumer's properties
        Map<String, Object> config = new HashMap<>();
        //add the Kafka server ip and the group id. the group id specifies the name
        //of the consumer group this consumer belongs to
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaDefaultGroupId);
        // the deserializer used for the key
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // the deserializer used for the value
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        // create a new DefaultKafkaConsumerFactory using the HashMap config inside the constructor
        return new DefaultKafkaConsumerFactory<>(config);
    }

    /**
     * Bean representing a {@link ConcurrentKafkaListenerContainerFactory} with a String key and a String value
     *
     * @return a {@link ConcurrentKafkaListenerContainerFactory} with a String key and a String value
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> stringKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(stringConsumerFactory());
        return factory;
    }


    /**
     * Bean representing a ConsumerFactory with a String key and a {@link User} value
     *
     * @return a {@link ConsumerFactory} with a String key and a {@link User} value
     */
    @Bean
    public ConsumerFactory<String, User> userConsumerFactory() {
        Map<String, Object> config = new HashMap<>();

        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaJsonGroupId);
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        /*
        notice that the DefaultKafkaConsumer used with JSON requires the config
        HashMap, plus a StringDeserializer (for the key), and a JsonDeserializer of type User,
        for the value
         */
        return new DefaultKafkaConsumerFactory<>(config,
                new StringDeserializer(),
                new JsonDeserializer<>(User.class));
    }


    /**
     * Bean representing a {@link ConcurrentKafkaListenerContainerFactory} with a String key and a {@link User} value
     *
     * @return a {@link ConcurrentKafkaListenerContainerFactory} with a String key and a {@link User} value
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, User> userKafkaListenerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, User> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(userConsumerFactory());
        return factory;
    }
}

