package com.example.simplespringkafka.config;

import com.example.simplespringkafka.model.User;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;


/**
 * Configuration for the Kafka Producer
 */
@Configuration
public class KafkaProducerConfiguration {


    @Value("${apache.kafka.server.address}")
    private String kafkaServerAddress;


    /**
     * Bean representing a  {@link ProducerFactory} with a String key and a  String value.
     *
     * @return a {@link ProducerFactory} with a String key and a String value.
     */
    @Bean
    public ProducerFactory<String, String> producerStringFactory() {
        Map<String, Object> config = new HashMap<>();
        // the Kafka server ip
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
        // the key type
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // the value type
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return new DefaultKafkaProducerFactory<>(config);
    }

    /**
     * Bean representing a  {@link ProducerFactory} with a String key and a {@link User} value.
     *
     * @return a {@link ProducerFactory} with a String key and a {@link User} value.
     */
    @Bean
    public ProducerFactory<String, User> producerUserFactory() {
        Map<String, Object> config = new HashMap<>();
        // the Kafka server ip
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
        // the key type
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        // the value type
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(config);
    }


    /**
     * Bean representing a  {@link KafkaTemplate} with a String key and a  {@link User} value
     *
     * @return a {@link KafkaTemplate} with a String key and a  {@link User} value
     */
    @Bean
    public KafkaTemplate<String, User> kafkaUserTemplate() {
        return new KafkaTemplate<>(producerUserFactory());
    }


    /**
     * Bean representing a  {@link KafkaTemplate} with a String key and a  String value.
     * @return a {@link KafkaTemplate} with a String key and a  String value.
     */
    @Bean
    public KafkaTemplate<String, String> kafkaStringTemplate() {
        return new KafkaTemplate<>(producerStringFactory());
    }


}
