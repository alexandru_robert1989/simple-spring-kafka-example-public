package com.example.simplespringkafka.resource;


import com.example.simplespringkafka.model.User;
import lombok.AllArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("kafka")
@AllArgsConstructor
public class KafkaProducer {


    /**
     * A {@link KafkaTemplate} using a String key and a String value
     */
    private final KafkaTemplate<String, String> kafkaStringTemplate;


    /**
     * A {@link KafkaTemplate} using a String key and a {@link User} value
     */
    private final KafkaTemplate<String, User> kafkaUserTemplate;


    /**
     * Hard coded String topic name
     */
    private static final String STRING_TOPIC = "testTopicString";


    /**
     * Hard coded {@link User} topic name
     */
    private static final String USER_TOPIC = "testTopicUser";



    @GetMapping("/publish/{message}")
    public String postString(@PathVariable String message) {
        kafkaStringTemplate.send(STRING_TOPIC, "Message sent to the '" + STRING_TOPIC + "' topic : '" + message + "'");
        return "Published:  " + message;

    }


    @GetMapping("/publish/future/{message}")
    public String postStringFuture(@PathVariable String message) {

        // the send method returns a ListenableFuture
        ListenableFuture<SendResult<String, String>> future = kafkaStringTemplate.send(STRING_TOPIC,
                "Message sent to the '" + STRING_TOPIC + "' topic : '" + message + "'");

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onFailure(Throwable throwable) {
                System.out.println("Sending message failed\nCause: " + throwable.getMessage());
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                System.out.println(" Message sent: " + message +
                        "\nOffset: " + result.getRecordMetadata().hasOffset() +
                        "\n\t\tRecord Metadata: " + result.getRecordMetadata() +
                        "\n\t\tProducer Metadata: " + result.getProducerRecord() +
                        "\n\t\tResult Metadata to String: " + result.toString());
            }
        });
        return "Published:  " + message;
    }


    @GetMapping("/publish-user/{name}/{dept}/{salary}")
    public String postUserMessage(@PathVariable String name, @PathVariable String dept, @PathVariable Long salary) {
        User user = new User(name, dept, salary);
        //   User user = new User(name, dept);
        kafkaUserTemplate.send(USER_TOPIC, user);
        return "created " + user.toString();

    }


}
