package com.example.simplespringkafka.resource;

import com.example.simplespringkafka.model.User;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "testTopicString", groupId = "group_id",
    containerFactory = "stringKafkaListenerContainerFactory")
    public void consume(String message) {
        System.out.println("\t\tConsumed message : " + message);
    }

    @KafkaListener(topics = "testTopicUser", groupId = "group_json",
            containerFactory = "userKafkaListenerFactory")
    public void consumeJson(User user) {
        System.out.println("Consumed JSON Message: " + user);
    }
}
